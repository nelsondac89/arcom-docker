#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <unistd.h>
#include <time.h>

#define MAX_STRING_SIZE 1024
//#define USERS_DIR = "./users/";
//#define ROOMS_DIR = "./rooms/";

#define TO "nelsondac89@gmail.com"
#define FROM "arcomefixe@gmail.com"

char USERS_DIR[] = "./users/";
char ROOMS_DIR[] = "./rooms/";

struct upload_status
{
  int lines_read;
};

//int create_listening_socket(int portno);
void error(char *msg);

void newUser(int argc, char *argv[]);
void checkDirAndCreate(char dirName[]);

void sendEmail(int argc, char *argv[]);
void newRoom(int argc, char *argv[]);
void newMessage(int argc, char *argv[]);
struct User GetUserData(char email[]);

struct User
{
  char name[64];
  char email[64];
};
struct Message {
  struct User Sender;
  time_t date;// = time(0);
  char message[512];
};

struct Room {
  char id[32];
  struct User User1;
  struct User User2;
  struct Message messages[256];
  int nMessages;
};

int main(int argc, char *argv[])
{    
  if (argc < 2)
  {
    fprintf(stderr, "ERROR, sem argumentos\n");
    exit(1);
  }
  if (strcmp(argv[1], "new_user") == 0)
  { //argv -- email nome
  printf("Lets go new user");
    newUser(argc, argv);
    //printf("Lets register new user - email %s, nome %s", argv[2], argv[3]);
  }
  else if (strcmp(argv[1], "new_room") == 0)
  {
    //printf("Lets go new room");
    newRoom(argc, argv);
  }
  else if (strcmp(argv[1], "new_message") == 0)
  {
    //printf("this is a new message");
    newMessage(argc, argv);
  }
  else if (strcmp(argv[1], "send_email") == 0)
  {
    sendEmail(argc, argv);
  }
  else
  {
    printf("nothing to do. %s | %s", argv[0], argv[1]);
  }

  printf("\n");
  return 0;
}

void newUser(int argc, char *argv[])
{
  struct User us;

  FILE *outfile;
  char fileName[128];

  if (argc < 4)
  {
    fprintf(stderr, "ERROR, newUser - sem argumentos\n");
    exit(1);
  }
  checkDirAndCreate(USERS_DIR);
  strcpy(us.email, argv[2]);
  strcpy(us.name, argv[3]);
  sprintf(fileName, "./users/%s.dat", us.email);
  outfile = fopen(fileName, "w");
  fwrite(&us, sizeof(struct User), 1, outfile);
  // close file
  fclose(outfile);
  //printf("Lets register new user - email %s, nome %s", argv[2], argv[3]);
  //printf("Struct - %s - %s ", us.email, us.name);
}

void newRoom(int argc, char *argv[])
{
  struct Room room;

  FILE *outfile;
  char fileName[128];
  
  if (argc < 5)
  {
    fprintf(stderr, "ERROR, newRoom - sem argumentos\n");
    exit(1);
  }  
  strcpy(room.id, argv[2]);  
  room.User1 = GetUserData( argv[3]);
  room.User2 = GetUserData( argv[4]);
  room.nMessages = 0;
  checkDirAndCreate(ROOMS_DIR);
  sprintf(fileName, "./rooms/%s.dat",  argv[2]); 
  outfile = fopen(fileName, "w");
  fwrite(&room, sizeof(struct Room), 1, outfile);
  // close file
  fclose(outfile);
}


struct User GetUserData(char email[])
{   
  char fileName[128];
  FILE *infile;
  struct User result;

  sprintf(fileName, "./users/%s.dat", email); 

  infile = fopen(fileName, "r");
  if (infile == NULL)
  {
    fprintf(stderr, "\nError opening file\n");
    exit(1);
  }
  while (fread(&result, sizeof(struct User), 1, infile));
  // close file
  fclose(infile);
  return result;
}

void newMessage(int argc, char *argv[])
{
  char fileName[128];
  FILE *infile;
  struct Room theRoom;
  strcpy(theRoom.id, argv[2]);
  sprintf(fileName, "./rooms/%s.dat", theRoom.id); 

  infile = fopen(fileName, "r");
  if (infile == NULL)
  {
    fprintf(stderr, "\nError opening file\n");
    exit(1);
  }
  while (fread(&theRoom, sizeof(struct Room), 1, infile));  
  fclose(infile);

  struct Message theMessage;
  theMessage.Sender = GetUserData(argv[3]);
  strcpy(theMessage.message,argv[4]);
  theMessage.date = time(0);  
  theRoom.messages[theRoom.nMessages] = theMessage;
  theRoom.nMessages+= 1;
  //write result to file
  infile = fopen(fileName, "w");
  fwrite(&theRoom, sizeof(struct Room), 1, infile);
  // close file
  fclose(infile);
}

void sendEmail(int argc, char *argv[])
{
  char result[MAX_STRING_SIZE] = "";
  int len = 0;
  char fileName[128];
  FILE *infile;
  struct User userData;
  struct Room roomData;

  sprintf(fileName, "./users/%s.dat", argv[3]); 

  infile = fopen(fileName, "r");
  if (infile == NULL)
  {
    fprintf(stderr, "\nError opening file 1\n");
    exit(1);
  }
  // read file contents till end of file
  while (fread(&userData, sizeof(struct User), 1, infile));
  // close file
  fclose(infile);

  sprintf(fileName, "./rooms/%s.dat",  argv[2]); 
  infile = fopen(fileName, "r");
  if (infile == NULL)
  {
    fprintf(stderr, "\nError opening file 2\n");
    exit(1);
  }  
  while (fread(&roomData, sizeof(struct Room), 1, infile));  
  fclose(infile);
  
  len += sprintf(result + len, "{\n\r\t\"user\": {\n\r");
  len += sprintf(result + len, "\t\t\"name\": \"%s\",\n\r", userData.name);
  len += sprintf(result + len, "\t\t\"email\": \"%s\"\n\r", userData.email);
  len += sprintf(result + len, "\t},\n\r");
  len += sprintf(result + len, "\t\"messages\": [\n\r");
  for (size_t i = 0; i < roomData.nMessages; i++)
  {
    if (i == 0)
    {
      //não escreve a virgula
      len += sprintf(result + len, "\t\t\"%s\"", roomData.messages[i].message);
    }
    else
    {
      len += sprintf(result + len, "\t\t,\"%s\"", roomData.messages[i].message);
    }
    
  }
  
  len += sprintf(result + len, "]");
  len += sprintf(result + len, "}");
  printf("\n\r%s\n\r", result);
}

void checkDirAndCreate(char dirName[])
{
  struct stat st = {0};
  if (stat(dirName, &st) == -1)
  {
    mkdir(dirName, 0700);
  }
}

void error(char *msg)
{
  perror(msg);
  exit(1);
}
