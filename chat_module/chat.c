#include <linux/module.h>
#include <linux/init.h>
#include <linux/fs.h>
#include <linux/version.h>
#include <linux/device.h>
#include <linux/cdev.h>
#include <linux/uaccess.h>

MODULE_AUTHOR("ARCOM 2018/19");
MODULE_LICENSE("GPL");

#define NDEVICES 2
#define DEVICE_NAME "arcom-chat"
#define DEVICE_FILE_NAME "arcom-chat"
#define CLASS "arcom-examples"
#define MAX_FILE_SIZE 256

static int size; //initialize to 0 in module_init
static int deviceOpen;
char data[MAX_FILE_SIZE];

static int __init arcom_chrdev_init(void);
static void __exit arcom_chrdev_exit(void);
static int arcom_chrdev_open(struct inode *inode, struct file *filp);
static int arcom_chrdev_release(struct inode *inode, struct file *filp);

static ssize_t arcom_chrdev_read(struct file *filp, char __user *buf, size_t count, loff_t *f_pos);
static ssize_t arcom_chrdev_write(struct file *filp, const char __user *buf, size_t count, loff_t *offset);

/* Exercicio 9 ******** */
typedef struct
{
 struct cdev arcom_cdev;
 char data[MAX_FILE_SIZE];
 int size;
 int deviceOpen;
 wait_queue_head_t WaitQ; //initialize with init_waitqueue_head
} arcom_chrdev_t;
static arcom_chrdev_t devices[NDEVICES];
/**/

DECLARE_WAIT_QUEUE_HEAD(myWaitQueue);

module_init(arcom_chrdev_init);
module_exit(arcom_chrdev_exit);

static struct class *arcom_class;
static dev_t dev_num;
struct cdev arcom_cdev[NDEVICES];

struct file_operations arcom_fops = {
  open : arcom_chrdev_open,
  release : arcom_chrdev_release,
  read : arcom_chrdev_read,
  write : arcom_chrdev_write,
};
static char *arcom_devnode(struct device *dev, umode_t *mode)
{
  if (!mode)
    return NULL;
  if (MAJOR(dev->devt) == MAJOR(dev_num))
    *mode = 0666;
  return NULL;
}

static int __init arcom_chrdev_init(void)
{
  int i;
  dev_t curr_dev;

  /* Request the kernel for NDEVICES minor numbers*/
  alloc_chrdev_region(&dev_num, 0, NDEVICES, DEVICE_NAME);

  /* Create a new class for this device. It will be visible in /sys/class */
  arcom_class = class_create(THIS_MODULE, CLASS);
  arcom_class->devnode = arcom_devnode;

  /* Create NDEVICES character devices (cdev) 
   * Each device will be accessible via /dev/arcom-chrdevX, where X 
   * is the respective minor number 
   */
  for (i = 0; i < NDEVICES; i++)
  {

    /* Tie file_operations to the cdev */
    cdev_init(&arcom_cdev[i], &arcom_fops);
    arcom_cdev[i].owner = THIS_MODULE;

    /* Device number to be associated to the current device */
    curr_dev = MKDEV(MAJOR(dev_num), MINOR(dev_num) + i);

    /* Activate the device */
    cdev_add(&arcom_cdev[i], curr_dev, 1);

    /* Device info can be viewed under /sys/class/arcom-example */
    device_create(arcom_class,
                  NULL, /* no parent device */
                  curr_dev,
                  NULL, /* no additional data */
                  DEVICE_FILE_NAME "%d", i);
  }

  pr_info("Module loaded\n");
  return 0;
}

static void __exit arcom_chrdev_exit(void)
{
  int i;
  dev_t curr_dev;

  unregister_chrdev_region(MKDEV(MAJOR(dev_num), MINOR(dev_num)), NDEVICES);

  for (i = 0; i < NDEVICES; i++)
  {
    curr_dev = MKDEV(MAJOR(dev_num), MINOR(dev_num) + i);
    device_destroy(arcom_class, curr_dev);
    cdev_del(arcom_cdev + i);
  }

  class_destroy(arcom_class);

  pr_info("Module unloaded\n");
}

int arcom_chrdev_open(struct inode *inode, struct file *filp)
{
  unsigned int maj = imajor(inode);
  unsigned int min = iminor(inode);
  arcom_chrdev_t *ptr;

  ptr = container_of(inode->i_cdev, arcom_chrdev_t, arcom_cdev);


  //ORIGINAL : n = copy_to_user(data + *offset, buf, i);
  //
  //ler dados do char
  //flip->coiso
  //escrever no private_data
  filp->private_data = ptr;
  

  pr_info("Device open\n");
  return 0;
}

int arcom_chrdev_release(struct inode *inode, struct file *filp)
{
  //module_put(THIS_MODULE);
  deviceOpen = 0;
  pr_info("Realasing...\n");
  return 0;
}
ssize_t arcom_chrdev_read(struct file *filp, char __user *buf, size_t count, loff_t *offset)
{
  int i, n;
  int last = *offset + count;
  
  //arcom_chrdev_t *ptr = filp->private_data;
  
  pr_warning("READING: size = %d", size);

  if (last > size)
    last = size;
  i = last - *offset;
  n = copy_to_user(data + *offset, buf, i);
  //n = copy_to_user(data, buf, i);
  *offset += i;
  if (n != 0)
    pr_warning("READING: Could not copy %d bytes\n", n);

  if (size < *offset)
    size = *offset;
  //pr_warning("Could not copy %d bytes\n", n);
  pr_info("fim da leitura");
  return i;
}

ssize_t arcom_chrdev_write(struct file *filp, const char __user *buf, size_t count, loff_t *offset)
{
  int i, n;
  int last = *offset + count;
  char msg[20] = "Isto é uma mensagem";
  arcom_chrdev_t *ptr = filp->private_data;
  //ptr->data = data + *offset;
  pr_info("print data - %s",data + *offset);
  // *personPtr, person1;
  

  if (last > MAX_FILE_SIZE)
    last = MAX_FILE_SIZE;
  pr_info("WRITTING: size = %d", last);
  
  i = last - *offset;
  
  copy_to_user(msg, buf, 20);

  n = copy_from_user(data + *offset, buf, i);
  *offset += i;
  if (n != 0)
    pr_warning("WRITE: Could not copy %d bytes\n", n);
 
  size = *offset;
  pr_info("finish write!" );
  return i;
}