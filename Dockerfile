FROM fedora

RUN yum -y update && yum clean all
RUN yum -y install nodejs nginx kmod && yum clean all


EXPOSE 8080
CMD ["/app/start.sh"]
ENTRYPOINT ["/app/start.sh"]
