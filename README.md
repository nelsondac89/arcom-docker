# Trabalho de ARCOM


## Objetivo
Criar uma instancia **docker**, com uma aplicação WEB de chat (**nodejs**) e com ligação a uma app em **C** que permita de alguma forma manipular e interagir com a app construida em node.

## Estrutura
* ./chat_message - a app do chat manager em c
* ./chat_webapp - a instancia em nodejs, usado o express como servidor e toda a comunicação entre clientes é feita por websockets (socket.io)


## chat_message
Esta aplicação será resposavel por receber e manipular dados em disco, e enviar por email.
* **new_user** [email] [nome]  - cria um ficheiro com os dados do utilizador que entra na aplicação.
O nome do ficheiro será o email do utilizador.
* **new_room** [room_id] [email_author] - cria um ficheiro com as informações da sala, neste caso com o id como nome do ficheiro. 
* **new_message** [room_id] [email_sender] [message] - abre o ficheiro da sala e incrementa os dados do vetor de mensagens da sala.
* **send_email**  [room_id] [email_toSend] - envia um email para a pessoa com as informações sobre os chats que esteve envolvido.

Para compilar o ficheiro basta executar o script ./chat.sh

## chat_webapp
Passos para iniciar:
* Ter o node instalado
* executar o npm install
* executar o npm start e a instancia abre na porta 8080
