const util = require('util');
const exec = util.promisify(require('child_process').exec);
const configs = require('../configs');
const nodemailer = require('nodemailer');

const getEmailContent = async (room,user) => {
  const theCommand = configs.appPath + " send_email \"" + room + "\" \"" + user +"\"";
  const { stdout, stderr } = await exec(theCommand);
  return stdout;
};

const sendEmail = (room,user) => {
  //const result = await getEmailContent();
  getEmailContent(room,user).then((x) => {
    console.log("promisse finish", x);
    const jsonR = JSON.parse(x.trim());
    var transporter = nodemailer.createTransport({
      service: 'gmail',
      auth: {
        user: configs.emailFrom,
        pass: configs.emailP
      }
    });

    const mailOptions = {
      from: "Arcom 2019/2020 <" + configs.emailFrom + ">", // sender address
      to: user, // list of receivers
      subject: 'Resumo conversa WEBCHAT', // Subject line
      html: '<p>Resumo conversa WEBCHAT</p>'// plain text body
    };

    mailOptions.html = `Utilizador: ${jsonR.user.name} (${jsonR.user.email}) <br />`;
    mailOptions.html += `Mensagens:<br />`;
    for (let index = 0; index < jsonR.messages.length; index++) {
      const element = jsonR.messages[index];
      mailOptions.html += `\t ${element}<br />`;
    }
    mailOptions.html += `-- FIM  DA CONVERSA --`;
    transporter.sendMail(mailOptions, function (err, info) {
      if (err)
        console.log(err)
      else
        console.log(info);
    });
  });
  return 'Ok';
};

exports.getEmailContent = getEmailContent;
exports.sendEmail = sendEmail;