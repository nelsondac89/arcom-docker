const util = require('util');
const exec = util.promisify(require('child_process').exec);
const configs = require('../configs');
const nodemailer = require('nodemailer');

const getMemStatus = async () => {
    const theCommand = "cat /proc/meminfo | grep MemFree | awk '{print $2}'";    
    const { stdout:memFree, stderr:memFreeError } = await exec(theCommand);

    const commandMemTotal = "cat /proc/meminfo | grep MemTotal | awk '{print $2}'"; 
    const { stdout:memTotal, stderr:memTotalError } = await exec(commandMemTotal);
     
    return {memFree: Math.round(memFree / 1024), memTotal: Math.round(memTotal / 1024)};
  };

  exports.getMemStatus = getMemStatus;

