const util = require('util');
const exec = util.promisify(require('child_process').exec);
const email = require('./events/sendEmail');
const memoryStats = require('./events/getMemoryStatus');

const configs = require('./configs');

var gravatar = require('gravatar');

async function register_user(email, name) {
    const theCommand = configs.appPath + " new_user " + '\"' + email + '\" \"' + name + '\"';
    const {
        stdout,
        stderr
    } = await exec(theCommand);
}

async function register_room(roomid, email, email2) {
    // ./chatmanager new_room 345 nelsondac89@gmail.com  aa
    
    const theCommand = configs.appPath + " new_room " + '\"' + roomid + '\" \"' + email + '\" \"' +email2 + '\"';
    const {
        stdout,
        stderr
    } = await exec(theCommand);
    //console.log('stdout:', stdout);
    //console.log('stderr:', stderr);
}

async function register_message(roomid, email, msg) {
    //./chatmanager new_message 345 nelsondac89@gmail.com "LINUX ON FIRE"           
    
    const theCommand = configs.appPath + " new_message " + '\"' + roomid + '\" \"' + email + '\" \"' + msg + '\"';
    const {
        stdout,
        stderr
    } = await exec(theCommand);
    //console.log('stdout:', stdout);
    //console.log('stderr:', stderr);
}


// Export a function, so that we can pass 
// the app and io instances from the app.js file:

module.exports = function(app, io) {

    app.get('/', function(req, res) {

        // Render views/home.html
        res.render('home');
    });

    app.get('/create', function(req, res) {

        // Generate unique id for the room
        var id = Math.round((Math.random() * 1000000));

        // Redirect to the random room
        res.redirect('/chat/' + id);
    });

    app.get('/chat/:id', function(req, res) {

        // Render the chant.html view
        res.render('chat');
    });

    // Initialize a new socket.io application, named 'chat'
    var chat = io.on('connection', function(socket) {        
        // When the client emits the 'load' event, reply with the 
        // number of people in this chat room
        const broadcastMemoryStats = () => 
        {
            memoryStats.getMemStatus().then(x => {               
                socket.broadcast.emit('memoryStats',x);
            })
        }
        const intervalMemory = setInterval(broadcastMemoryStats, 3000);
        socket.on('load', function(data) {

            var room = findClientsSocket(io, data);
            if (room.length === 0) {
                socket.emit('peopleinchat', {
                    number: 0
                });
            } else if (room.length === 1) {
                socket.emit('peopleinchat', {
                    number: 1,
                    user: room[0].username,
                    avatar: room[0].avatar,
                    id: data
                });
            } else if (room.length >= 2) {

                chat.emit('tooMany', {
                    boolean: true
                });
            }
        });

        // When the client emits 'login', save his name and avatar,
        // and add them to the room
        socket.on('login', function(data) {
            var room = findClientsSocket(io, data.id);
            // Only two people per room are allowed
            if (room.length < 2) {

                // Use the socket object to store data. Each client gets
                // their own unique socket object
                register_user(data.email, data.user);                
                socket.username = data.user;
                socket.room = data.id;
                socket.email = data.email;
                socket.avatar = gravatar.url(data.avatar, {
                    s: '140',
                    r: 'x',
                    d: 'mm'
                });

                // Tell the person what he should use for an avatar
                socket.emit('img', socket.avatar);

                // Add the client to the room
                socket.join(data.id);

                if (room.length == 1) {
                    register_user(data.email, data.user);    
                    var usernames = [],
                        avatars = [],
                        emails = [];

                    usernames.push(room[0].username);
                    usernames.push(socket.username);

                    avatars.push(room[0].avatar);
                    avatars.push(socket.avatar);

                    emails.push(room[0].email);
                    emails.push(socket.email);

                    register_room(data.id, emails[0], emails[1]);
                    // Send the startChat event to all the people in the
                    // room, along with a list of people that are in it.

                    chat.in(data.id).emit('startChat', {
                        boolean: true,
                        id: data.id,
                        users: usernames,
                        avatars: avatars,
                        emails: emails
                    });
                }
            } else {
                socket.emit('tooMany', {
                    boolean: true
                });
            }
        });

        // Somebody left the chat
        socket.on('disconnect', function() {

            // Notify the other person in the chat room
            // that his partner has left

            socket.broadcast.to(this.room).emit('leave', {
                boolean: true,
                room: this.room,
                user: this.username,
                avatar: this.avatar
            });

            // leave the room
            socket.leave(socket.room);
        });


        // Handle the sending of messages
        socket.on('msg', function(data) {                    
            register_message(socket.room, data.email, data.msg);
            // When the server receives a message, it sends it to the other person in the room.
            socket.broadcast.to(socket.room).emit('receive', {
                msg: data.msg,
                user: data.user,
                img: data.img
            });
        });

        socket.on('sendEmail', function(data) {                    
        email.sendEmail(socket.room, data.email);
          
        });


    });
};

function findClientsSocket(io, roomId, namespace) {
    var res = [],
        ns = io.of(namespace || "/"); // the default namespace is "/"

    if (ns) {
        for (var id in ns.connected) {
            if (roomId) {
                var index = ns.connected[id].rooms.indexOf(roomId);
                if (index !== -1) {
                    res.push(ns.connected[id]);
                }
            } else {
                res.push(ns.connected[id]);
            }
        }
    }
    return res;
}